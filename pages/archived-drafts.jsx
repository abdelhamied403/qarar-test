import React from 'react';
import Head from 'next/head';
import ArchivedDraftsPage from '../views/Pages/archived-drafts/archived-drafts';

const ArchivedDrafts = () => (
  <div>
    <Head>
      <title>المسودات مؤرشفة - قرار - وزارة الشؤون البلدية والقروية</title>
    </Head>
    <ArchivedDraftsPage />
  </div>
);

export default ArchivedDrafts;
